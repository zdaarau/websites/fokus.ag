+++
title = "Momentan _keine_ Umfrage im Gange"
keywords = []
draft = true
+++

Zur Zeit findet keine Befragung zu kantonalen Abstimmungen oder Wahlen statt. [Sollte es der Zufall so wollen](/informationen/#faq-befragte-2), werden Sie beim nächsten Mal eine Einladung zur Teilnahme erhalten.

Detaillierte Informationen zum Projekt _FOKUS Aargau_ finden Sie [hier](/informationen).

<span class="text-info">**Vielen Dank für Ihr Interesse.**</span>

<br>

<a href="/" class="btn btn-small btn-template-main"><i class="icon-home"></i> Zur Startseite</a>

<br>

