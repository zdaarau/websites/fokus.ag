+++
title = "Informationen"
icon_classes = "icon-help-circled iconleftinline iconuptinybit"
keywords = ["FAQ", "Informationen", "Teilnahme", "wieso", "warum", "wer", "was"]
+++

### Das Projekt _FOKUS Aargau_

Ziel des Projektes _FOKUS Aargau_ ist es, nach **kantonalen** Abstimmungen und Wahlen herauszufinden, was die Aargauer Bürgerinnen und Bürger zu ihrem Entscheid bewogen hat bzw. weshalb sich diese ihrer Stimme enthalten haben. Im Fokus steht dabei der individuelle Meinungsbildungsprozess. Dabei handelt es sich um eine Premiere: Bisher gab es noch in keinem Kanton eine solche systematische Erhebung im Nachgang zu kantonalen Abstimmungen oder Wahlen.

Demgegenüber werden bereits seit 1977 nach allen **eidgenössischen** Volksabstimmungen im Auftrag des Bundesrates Nachbefragungen durchgeführt – früher bekannt unter dem Namen _VOX_, heute {{< a_blank "_VOTO_" "http://www.voto.swiss/" >}}. Zweck dieser Umfragen ist es ebenfalls, die Motive der Stimmbürgerinnen und Stimmbürger für ihr Ja bzw. Nein an der Urne besser zu verstehen. FOKUS Aargau ist in gewissem Sinne das kantonale Äquivalent zur nationalen VOTO-Studie.

FOKUS Aargau deckt etwa jeden zweiten Urnengang ab. Somit werden rund zwei Mal jährlich zwischen 1'000–2'000 zufällig ermittelte Aargauer Stimmberechtigte befragt. Sie werden per Post informiert und können online oder mit einem gedruckten Fragebogen teilnehmen. Die Antworten dienen ausschliesslich wissenschaftlichen Zwecken und das {{< a_blank "Zentrum für Demokratie Aarau (ZDA)" "https://www.zdaarau.ch/" >}} wertet nur anonymisierte Daten aus. Alle Personendaten werden nach Abschluss der Befragung unwiderruflich gelöscht.

Die Befragung führt aktuell das Umfrageinstitut {{< a_blank "DemoSCOPE" "https://www.demoscope.ch/" >}} im Auftrag des ZDA durch. Das ZDA analysiert die Umfrageergebnisse nach strengen wissenschaftlichen Standards und verfasst einen für die Öffentlichkeit [frei zugänglichen Bericht](/analysen/), welcher allen Interessierten erlaubt, die Hintergründe des Volkswillens besser zu verstehen.

Im Vordergrund stehen dabei Fragen wie:

- Was waren die Hauptmotive für ein Ja oder Nein an der Urne?
- Welche Argumente dominierten den Abstimmungskampf?
- Wie gut informiert waren die Teilnehmenden über die Abstimmungsvorlagen?
- Welche Alters- Bildungs- oder Einkommensgruppen entscheiden sich wie?
- Welchen Einfluss spielen psychologische Komponenten beim Stimmentscheid?
- Zeigen sich regionale Unterschiede in den Beteiligungs- und Zustimmungsraten?

Finanziert wird die Studie aus dem {{< a_blank "kantonalen Swisslos-Fonds" "https://www.ag.ch/de/dfr/finanzen/swisslos_fonds_unterstuetzungsbeitraege_beantragen_/swisslos_fonds_unterstuetzungsbeitraege_beantragen_1.jsp" >}}.

Im Folgenden finden sich häufig gestellte Fragen und unsere dazugehörigen Antworten. Sollten trotzdem noch Fragen offen bleiben, dürfen Sie uns natürlich gerne kontaktieren.

<a href="/kontakt" class="btn btn-small btn-template-main">Zur Kontaktseite</a>

---

### Häufig gestellte Fragen und Antworten

#### Allgemein

{{< accordion id="faq-allgemein" data="faq-allgemein" >}}

---

#### Für Befragungs­teilnehmer/­innen

{{< accordion id="faq-befragte" data="faq-befragte" >}}
