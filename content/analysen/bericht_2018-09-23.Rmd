---
title: "FOKUS Aargau Nr. 1: Millionärs\u00adsteuer\u00adinitiative"
authors: ["Thomas Milic", "Salim Brüggemann", "Uwe Serdült"]
lead: "Zwei Drittel aller Stimmenden befand die Millionärs\u00adsteuer\u00adinitiative als nicht geeignet zur Sanierung der Kantons\u00adfinanzen. Und: 86\u202f% der Stimm\u00adbevölkerung informierte sich mit dem kantonalen Stimm\u00adbüchlein über die Vorlage. Dies zeigt die Analyse der Befragung von 1'339 Stimm\u00adberechtigten im Rahmen der Studie «FOKUS Aargau» zur kantonalen Abstimmung vom 23. September 2018. Die Studie wurde vom Zentrum für Demokratie Aarau und vom Befragungs\u00adinstitut publitest erstmals durchgeführt und vom Swisslos-Fonds Kanton Aargau unterstützt."
date: 2018-11-06T08:00:00+01:00
vote_date: "2018-09-23"
categories: ["Berichte"]
tags: ["Abstimmung", "23. September 2018"]
keywords: ["Millionärssteuerinitiative"]
banner_prefix: "img/banners/millionaire"
show_banner_in_post: true
unsplash_id: "uxHxilXLKmA"
draft: false
---

```{r, summary, child = fs::path_join(c(getOption("path.repo.fa.fokus_aargau"), "rmd_snippets/2018-09-23_summary.Rmd"))}
```

**Hier geht's zum ganzen [Bericht](/FOKUS_Aargau_Bericht_2018-09-23.pdf) sowie dem
[Fragebogen](https://gitlab.com/zdaarau/fokus_reports/-/raw/main/print_docs/aargau/questionnaire_print_2018-09-23.pdf?ref_type=heads&inline=true)**:

<a href="/FOKUS_Aargau_Bericht_2018-09-23.pdf"><img src="/img/thumbnails/thumbnail_report_2018-09-23.png" alt="FOKUS_Aargau_Bericht_2018-09-23.pdf" class="pdf-thumbnail"/></a><a href="https://gitlab.com/zdaarau/fokus_reports/-/raw/main/print_docs/aargau/questionnaire_print_2018-09-23.pdf?ref_type=heads&inline=true"><img src="/img/thumbnails/thumbnail_questionnaire_2018-09-23.png" alt="FOKUS Aargau Print-Fragebogen 2018-09-23 (PDF)" class="pdf-thumbnail"/></a>
