---
title: "FOKUS Aargau Nr. 3: Regierungsrats\u00adersatz\u00adwahl vom 20. Oktober 2019"
authors: ["Uwe Serdült", "Thomas Milic", "Salim Brüggemann"]
lead: ""
date: 2019-11-20T12:00:00+01:00
vote_date: "2019-10-20"
sub_publication: "cantonal_majoritarian"
sub_publication_title: "Regierungsratsersatzwahl"
categories: ["Berichte"]
tags: ["Wahlen", "20. Oktober 2019"]
keywords: ["Regierungsrat"]
banner_prefix: "img/banners/government_building_aarau_2"
banner_credits:
  url: "https://commons.wikimedia.org/wiki/File:Aarau-Regierungsgebaeude.jpg"
  title: "Aarau-Regierungsgebaeude"
  creator: "Roland Zumbuehl"
  license_url: "https://creativecommons.org/licenses/by-sa/3.0/"
  license_string: "<i class='icon-cc iconflushbegin'></i><i class='icon-cc-by iconflush'></i><i class='icon-cc-sa iconflushend'></i>3.0"
show_banner_in_post: true
draft: false
---

```{r, summary, child = fs::path_join(c(getOption("path.repo.fa.fokus_aargau"), "rmd_snippets/2019-10-20_cantonal_majoritarian_summary.Rmd"))}
```

**Hier geht's zum ganzen [Bericht](/FOKUS_Aargau_Bericht_Regierungsratsersatzwahl_2019-10-20.pdf) sowie dem
[Fragebogen](https://gitlab.com/zdaarau/fokus_reports/-/raw/main/print_docs/aargau/questionnaire_print_2019-10-20.pdf?ref_type=heads&inline=true)**:

<a href="/FOKUS_Aargau_Bericht_Regierungsratsersatzwahl_2019-10-20.pdf"><img src="/img/thumbnails/thumbnail_report_2019-10-20_cantonal_majoritarian.png" alt="FOKUS_Aargau_Bericht_Regierungsratsersatzwahl_2019-10-20.pdf" class="pdf-thumbnail"/></a><a href="https://gitlab.com/zdaarau/fokus_reports/-/raw/main/print_docs/aargau/questionnaire_print_2019-10-20.pdf?ref_type=heads&inline=true"><img src="/img/thumbnails/thumbnail_questionnaire_2019-10-20.png" alt="FOKUS Aargau Print-Fragebogen 2019-10-20 (PDF)" class="pdf-thumbnail"/></a>
