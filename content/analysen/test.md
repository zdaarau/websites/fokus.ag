+++
title = "Test Post"
authors = ["Zentrum für Demokratie Aarau (ZDA)"]
date = 2020-06-17T12:00:00+01:00
tags = ["test", "playground"]
categories = ["draft"]
banner_prefix = "img/banners/millionaire"
show_banner_in_post = true
draft = true
+++

## Ordentliche Berichte

Jeweils innerhalb von 5–6 Wochen nach einem von FOKUS Aargau abgedeckten Abstimmungs- oder Wahltermin werden die Ergebnisse unserer repräsentativen Bevölkerungsumfrage in Form eines ausführlichen Berichtes veröffentlicht. Für alle, die es eilig haben, wird eine separate Kurzversion dieses Berichtes die wichtigsten Punkte zusammenfassen.

## Ergänzungen und Ad-hoc-Analysen

Daneben publiziert unser Team in unregelmässigen Abständen zusätzliche Analysen, welche nicht ins Konzept der ordentlichen Abstimmungsberichte passen. Diese können etwa einen bestimmten, interessanten Aspekt des Aargauer Politik beleuchten oder es handelt sich um ergänzende Auswertungen, welche den Rahmen der ordentlichen Berichte sprengen würden. Alle solchen Analysen werden auch unter [analysen.fokus.ag](https://analysen.fokus.ag/) zu finden sein.

<br>

_**Vielen Dank für Ihr Interesse.**_

<br>

## FOKUS Aargau: Abzudeckende Abstimmungs-/Wahltermine

### Allgemeine Hinweise

- Die regulären, vierteljährlichen Termine zukünftiger Abstimmungen und Wahlen (“Blanko-Abstimmungstermine”) finden sich auf [dieser Webseite](https://www.bk.admin.ch/ch/d/pore/va/vab_1_3_3_1.html) der Bundeskanzlei. Informationen zu zukünftigen Abstimmungsterminen seitens des Kantons AG finden sich [hier](https://www.ag.ch/de/weiteres/aktuelles/wahlen_und_abstimmungen/abstimmungen/abstimmungen.jsp).

- Eine Übersicht hinterlegter und hängiger Volksinitiativen im Kanton AG findet sich [hier](https://www.ag.ch/de/weiteres/aktuelles/wahlen_und_abstimmungen/volksinitiativen/aktuelle_und_haengige_initiativen/aktuelle_und_haengige_initiativen.jsp), hängige Referenden [hier](https://www.ag.ch/de/weiteres/aktuelles/wahlen_und_abstimmungen/referenden/haengige_referenden/haengige_referenden.jsp).

- Gemäss der Aargauer Staatskanzlei (Besprechung vom 13. März 2018) ist jeweils erst rund 3 bis 4 Monate vor einem Abstimmungstermin klar, welche Vorlagen tatsächlich zur Abstimmung kommen. Die Abstimmungsbroschüre mit allen behördlichen Informationen wird jeweils rund 2 Monate vor dem Abstimmungstermin fertiggestellt.


### Terminübersicht


| #   | Termin | Typ | Gegenstand | Anmerkungen |
| :-- | :----: | :-- | :--------- | :---------- |
| 1 | 2018-09-23 | kantonale Volksinitiative | [Millionärssteuer ─ Für eine faire Vermögenssteuer im Aargau](https://www.millionaerssteuer.ch/de/) | Die Initiative wird [ohne Gegenvorschlag](https://www.ag.ch/grossrat/grweb/de/147/Startseite?FrmEntity=grweb.modules.dok.GrDok&FrmRequest=Download&DokId=3763255&Extension=.pdf) zur Abstimmung kommen.<br><br>[Geschäft](https://www.ag.ch/grossrat/grweb/de/195/Detail%20Gesch%C3%A4ft?ProzId=3630430) im Aargauer Grossrat
| 1 | 2018-09-23 | direkter Gegenentwurf zu einer eidgenössischen Volksinitiative | [Bundesbeschluss vom 13. März 2018 über die Velowege sowie die Fuss- und Wanderwege (direkter Gegenentwurf zur Volksinitiative «Zur Förderung der Velo-, Fuss- und Wanderwege [Velo-Initiative]»)](https://www.admin.ch/opc/de/federal-gazette/2018/1483.pdf) | Eine Übersicht über die “Stationen” der Velo-Initiative der BK findet sich [hier](https://www.bk.admin.ch/ch/d/pore/vi/vis459.html). |
| 1 | 2018-09-23 | eidgenössische Volksinitiative | [Volksinitiative vom 26. November 2015 «Für gesunde sowie umweltfreundlich und fair hergestellte Lebensmittel (Fair-Food-Initiative)»](https://de.wikipedia.org/wiki/Eidgen%C3%B6ssische_Volksinitiative_%C2%ABF%C3%BCr_gesunde_sowie_umweltfreundlich_und_fair_hergestellte_Lebensmittel_(Fair-Food-Initiative)%C2%BB) | |
| 1 | 2018-09-23 | eidgenössische Volksinitiative | [Volksinitiative vom 30. März 2016 «Für Ernährungssouveränität. Die Landwirtschaft betrifft uns alle»](http://www.xn--ernhrungssouvernitt-iwbmd.ch/) | |
| 2 | 2018-11-25 | kantonale Volksinitiative | [JA! für euse Wald](http://www.jafuereusewald.ch/wp/) | Nach dem [Regierungsrat](https://www.srf.ch/news/regional/aargau-solothurn/steuergeld-fuer-den-wald-aargauer-regierungsrat-lehnt-wald-initiative-ab) ist auch die [vorberatende Grossratskommission](https://www.aargauerzeitung.ch/aargau/kanton-aargau/keine-bessere-entschaedigung-fuer-waldbesitzer-kommission-lehnt-initiative-ab-132323827) gegen die Unterbreitung eines Gegenvorschlags.<br><br>[Geschäft](https://www.ag.ch/grossrat/grweb/de/195/Detail%20Gesch%C3%A4ft?ProzId=3688869) im Aargauer Grossrat|
| 2 | 2018-11-25 | kantonales obligatorisches Referendum | Teilnahme an Ständeratswahlen für AuslandschweizerInnen | SVP und EDU [sind dagegen](https://www.ag.ch/de/weiteres/aktuelles/medienportal/medienmitteilung/medienmitteilungen/mediendetails_90698.jsp) (22.12.2017)<br><br>Grossrätliche Kommission [unterstützt](https://www.ag.ch/de/weiteres/aktuelles/medienportal/medienmitteilung/medienmitteilungen/mediendetails_92101.jsp) Teilnahme der Auslandschweizerinnen und Auslandschweizer an Ständeratswahlen (18.01.2018)<br><br>[Geschäft](https://www.ag.ch/grossrat/grweb/de/195/Detail%20Gesch%C3%A4ft?ProzId=3684950) im Aargauer Grossrat |
| 2 | 2018-11-25 | eidgenössische Volksinitiative | [Volksinitiative «Für die Würde der landwirtschaftlichen Nutztiere (Hornkuh-Initiative)»](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20181125/hornkuh-initiative.html) | |
| 2 | 2018-11-25 | eidgenössische Volksinitiative | [Volksinitiative «Schweizer Recht statt fremde Richter (Selbstbestimmungsinitiative)»](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20181125/selbstbestimmungsinitiative.html) | |
| 2 | 2018-11-25 | eidgenössisches fakultatives Referendum | [Änderung des Bundesgesetzes über den Allgemeinen Teil des Sozialversicherungsrechts (ATSG) (Gesetzliche Grundlage für die Überwachung von Versicherten)](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20181125/uberwachung-versicherte.html) | |
| 3 | 2019-10-20 | eidgenössische Wahl | [Gesamterneuerungswahl des Nationalrats](https://www.ag.ch/de/weiteres/aktuelles/wahlen_und_abstimmungen/wahlen/archiv_2/archiv_details_2/archiv_details_112069.jsp) | |
| 3 | 2019-10-20 | eidgenössische Wahl | [Gesamterneuerungswahl des Ständerats](https://www.ag.ch/de/weiteres/aktuelles/wahlen_und_abstimmungen/wahlen/archiv_2/archiv_details_2/archiv_details_112067.jsp) | |
| 3 | 2019-10-20 | kantonale Wahl | [Ersatzwahl eines Mitglieds des Regierungsrats](https://www.ag.ch/de/aktuelles/wahlen_und_abstimmungen/wahlen/archiv_2/archiv_details_2/archiv_details_124831.jsp) | |
| 4 | 2020-09-27 | kantonales obligatorisches Referendum | [Änderung der Verfassung des Kantons Aargau (Neuorganisation der Führungsstrukturen der Aargauer Volksschule)](https://www.ag.ch/de/bks/ueber_uns_bks/dossiers_projekte_bks/optimierung_fuehrungsstrukturen/optimierung_fuehrungsstrukturen.jsp) | Es handelt sich im Wesentlichen um die Abschaffung der Schulpflege.<br><br>An diesem Datum finden zugleich noch die [Gesamterneuerungswahlen der Bezirks- und Kreisbehörden](https://www.ag.ch/de/aktuelles/wahlen_und_abstimmungen/wahlen/vorschau_wahlen/vorschau_details_wahlen/vorschau_details_wahlen_135557.jsp) statt |
| 4 | 2020-09-27 | kantonales Behördenreferendum | [Änderung des Schulgesetzes (Neuorganisation der Führungsstrukturen der Aargauer Volksschule)](https://nein-zur-abschaffung-der-schulpflege.ch/) | Es handelt sich im Wesentlichen um die Abschaffung der Schulpflege. |
| 4 | 2020-09-27 | kantonales Behördenreferendum | [Änderung des Energiegesetzes des Kantons Aargau (EnergieG)](https://www.ag.ch/de/aktuelles/wahlen_und_abstimmungen/abstimmungen/vorschau/vorschau_details/vorschau_details_140482.jsp?tabId=1&sectionId=216143) | |
| 4 | 2020-09-27 | eidgenössische Volksinitiative | [Volksinitiative «Für eine massvolle Zuwanderung (Begrenzungsinitiative)»](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20200927.html) | |
| 4 | 2020-09-27 | eidgenössisches fakultatives Referendum | [Änderung des Bundesgesetzes über die Jagd und den Schutz wildlebender Säugetiere und Vögel (Jagdgesetz, JSG)](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20200927.html) | [Nein-Komittee](https://jagdgesetz-nein.ch/) |
| 4 | 2020-09-27 | eidgenössisches fakultatives Referendum | [Änderung des Bundesgesetzes über die direkte Bundessteuer (Steuerliche Berücksichtigung der Kinderdrittbetreuungskosten)](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20200927.html) | |
| 4 | 2020-09-27 | eidgenössisches fakultatives Referendum | [Änderung des Bundesgesetzes über den Erwerbsersatz für Dienstleistende und bei Mutterschaft (indirekter Gegenvorschlag zur Volksinitiative «Für einen vernünftigen Vaterschaftsurlaub – zum Nutzen der ganzen Familie»)](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20200927.html) | |
| 4 | 2020-09-27 | eidgenössisches fakultatives Referendum | [Bundesbeschluss über die Beschaffung neuer Kampfflugzeuge](https://www.admin.ch/gov/de/start/dokumentation/abstimmungen/20200927.html) | |
| 5 | 2020-10-18 | kantonale Wahl | [Gesamterneuerungswahl des Grossen Rats](https://www.ag.ch/de/aktuelles/wahlen_und_abstimmungen/wahlen/vorschau_wahlen/vorschau_details_wahlen/vorschau_details_wahlen_135554.jsp) | |
| 5 | 2020-10-18 | kantonale Wahl | [Gesamterneuerungswahl des Regierungsrats](https://www.ag.ch/de/aktuelles/wahlen_und_abstimmungen/wahlen/vorschau_wahlen/vorschau_details_wahlen/vorschau_details_wahlen_135556.jsp) | |

![blaue Fackel](/img/background_ongoing.jpg)

## Kontakt Kanton Aargau

Der Kontakt läuft über die [Staatskanzlei](https://www.ag.ch/de/sk/sk.jsp) bzw. dessen [kantonales Wahlbüro](https://www.ag.ch/de/meta/kontakt/staatskalender/staatskalender.jsp?rub=13412&CFID=214462445&CFToken=24544648)[^general-email]. Für die Stichprobenziehung ist die Abteilung [Statistik Aargau](https://www.ag.ch/de/meta/kontakt/staatskalender/staatskalender.jsp?rub=12749&CFID=5064862&CFToken=38) zuständig.

AnsprechpartnerInnen sind:

- Urs Meier, Leiter des Wahlbüros
- [Anina Sax](mailto:anina.sax@ag.ch), stellvertretende Leiterin des Wahlbüros
- [Annina Zimmerli](mailto:annina.zimmerli@ag.ch), Projektleiterin Wahlen und Abstimmungen
- [Dr. Andrea Regina Plüss](mailto:andrea.pluess@ag.ch), Abteilungsleiterin Statistik Aargau
- [Ruedi Steiner](mailto:ruedi.steiner@ag.ch), wissenschaftlicher Mitarbeiter Statistik Aargau; tel.: +41 62 835 13 03

Ein Organigramm des Generalsekretariats der Aargauer Staatskanzlei findet sich [hier](https://web.archive.org/web/20180325195757/https://www.ag.ch/media/kanton_aargau/sk/dokumente_7/ueber_uns_5/organisation_9/organigramme_2/Organigramm_SK_Generalsekretariat.pdf).

[^general-email]: Die allgemeine E-Mail-Adresse des Wahlbüros lautet: [`wahlbuero@ag.ch`](mailto:wahlbuero@ag.ch)

![Hi there!](/img/background_ongoing_3.jpg)
