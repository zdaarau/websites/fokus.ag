+++
title = "Hier finden Sie in Zukunft unsere Publikationen"
authors = ["Zentrum für Demokratie Aarau (ZDA)"]
date = 2018-09-17T12:00:00+01:00
expiryDate = 2018-11-06T08:00:00+01:00
tags = []
categories = []
banner = "img/clients/ZDA_Logo.svg"
draft = false
+++

## Ordentliche Berichte

Jeweils innerhalb von 5–6 Wochen nach einem von FOKUS Aargau abgedeckten Abstimmungs- oder Wahltermin werden die Ergebnisse unserer repräsentativen Bevölkerungsumfrage in Form eines ausführlichen Berichtes veröffentlicht. Für alle, die es eilig haben, wird eine separate Kurzversion dieses Berichtes die wichtigsten Punkte zusammenfassen.

## Ergänzungen und Ad-hoc-Analysen

Daneben publiziert unser Team in unregelmässigen Abständen zusätzliche Analysen, welche nicht ins Konzept der ordentlichen Abstimmungsberichte passen. Diese können etwa einen bestimmten, interessanten Aspekt des Aargauer Politik beleuchten oder es handelt sich um ergänzende Auswertungen, welche den Rahmen der ordentlichen Berichte sprengen würden. Alle solchen Analysen werden auch unter [analysen.fokus.ag](https://analysen.fokus.ag/) zu finden sein.

<br>

_**Vielen Dank für Ihr Interesse.**_

<br>
