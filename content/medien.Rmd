---
title: "Medienspiegel"
icon_classes: "icon-newspaper iconleftinline"
keywords: ["Medien", "Media", "Medienspiegel", "Media Coverage", "Berichterstattung", "Zeitungsartikel"]
draft: false
output:
  blogdown::html_page:
    md_extensions: "-autolink_bare_uris"
---

```{r, echo = FALSE}
# fetch relevant library collection
data_media_coverage <-
  zoterro::items(user = zoterro::zotero_group_id(4618960),
                 collection_key = "2JGIFP4E",
                 incl_children = TRUE) %>%
  zoterro::add_archive_url() %>%
  dplyr::filter(itemType != "attachment") %>%
  # add proper date type and itemType-independent `medium` col
  dplyr::mutate(date = clock::date_parse(date),
                medium = dplyr::case_when(itemType == "newspaperArticle" ~ publicationTitle,
                                          itemType == "radioBroadcast" ~ paste0(programTitle, ", ", network),
                                          itemType == "webpage" ~ websiteTitle)) %>%
  # arrange by date and prettify date
  dplyr::arrange(desc(date)) %>%
  dplyr::mutate(date = salim::phrase_datetime(x = date,
                                              locale = "de-CH")) %>%
  # export relevant cols to CSV file^
  dplyr::select(date,
                medium,
                title,
                url,
                archiveUrl) %T>%
  readr::write_csv(file = "../static/data/media_coverage.csv",
                   na = "")

# create HTML table
htmltools::tags$table(
  class = "table-media-coverage",
  htmltools::tags$caption("Bisherige Medienberichterstattung"),
  htmltools::tags$thead(
    htmltools::tags$tr(
      htmltools::tags$th(id = "col_date",
                         "Datum"),
      htmltools::tags$th(id = "col_medium",
                         "Medium"),
      htmltools::tags$th(id = "col_item",
                         "Beitrag")
    )
  ),
  htmltools::tags$tbody(
    data_media_coverage %>%
      purrr::pmap(~ htmltools::tags$tr(
        htmltools::tags$td(headers = "col_date",
                           ..1),
        htmltools::tags$td(headers = "col_medium",
                           title = ..2,
                           ..2 %>%
                             stringr::str_replace(pattern = ", Schweizer Radio und Fernsehen \\(SRF\\)",
                                                  replacement = ", SRF") %>%
                             stringr::str_trunc(width = 50L)),
        htmltools::tags$td(headers = "col_item",
                           title = ..3,
                           htmltools::a(href = ..4,
                                        target = "_blank",
                                        rel = "noopener",
                                        stringr::str_trunc(string = ..3,
                                                           width = 100L)),
                           if (!is.na(..5)) htmltools::a(href = ..5,
                                                         
                                                         target = "_blank",
                                                         rel = "noopener",
                                                         "\u221E"))
      ))
  )
)
```
