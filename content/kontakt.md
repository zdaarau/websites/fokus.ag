+++
title = "Kontakt"
icon_classes = "icon-mail-alt iconflushleft iconuptinybit marginrightbit"
id = "contact"
keywords = ["Kontakt", "contact", "Adresse", "address", "Kontaktierung", "kontaktieren", "Ansprechpartner"]
+++

# Haben Sie Fragen?

Möchten Sie mit jemandem vom Projekt _FOKUS Aargau_ persönlich sprechen? Sind Sie nicht aus dem Aargau, interessieren sich aber für eine Ausweitung des Projektes auf Ihren Heimatkanton? Zögern Sie nicht, uns zu kontaktieren. Schreiben Sie uns Ihr Anliegen über untenstehendes Formular oder direkt per E-Mail[^pgp] an [<span class="protected-e-mail" data-user="ofni" data-website="ga.sukof"></span>](mailto:%69%6E%66%6F%40%66%6F%6B%75%73%2E%61%67)&#x20;und wir werden uns sobald wie möglich bei Ihnen melden.

<div class="box-simple box-dark box-left-align">
<h4 class="just-white">Für Befragungs­teilnehmer/­innen</h4>

Sofern Sie als Teilnehmer/in bei FOKUS Aargau Hilfe beim Ausfüllen des Fragebogens, technische Unterstützung oder Ähnliches benötigen, benützen Sie bitte <strong><em>nicht</em></strong> das untenstehende Formular, sondern wenden Sie sich stattdessen direkt an das zuständige Befragungsinstitut DemoSCOPE unter <a href="mailto:%74%65%63%68%40%64%65%6D%6F%73%63%6F%70%65%2E%73%77%69%73%73"><span class="protected-e-mail" data-user="hcet" data-website="ssiws.epocsomed"></span></a>.
</div>

[^pgp]: Sie erreichen uns auch auf vertraulichem/verschlüsseltem Wege ({{< a_blank_ns "PGP/MIME" "https://de.wikipedia.org/wiki/PGP/MIME"  >}}). Der öffentliche OpenPGP-Schlüssel für unsere E-Mail-Adresse findet sich [hier als Datei](/pgp_pub_key.asc) oder – ideal für Mobilgeräte – [als QR-Code](https://keyoxide.org/util/qr/OPENPGP4FPR%3A707780F17038521B6738D3D2EAD7FB9701A49D7A).
