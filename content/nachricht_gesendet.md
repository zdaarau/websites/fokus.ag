+++
title = "Ihre Nachricht wurde erfolgreich übermittelt"
[image_credits]
url = "https://commons.wikimedia.org/wiki/File:Paper_Plane_Vector.svg"
title = "Paper Plane"
creator = "VideoPlasty"
license_url = "https://creativecommons.org/licenses/by-sa/4.0/"
license_string = "<i class='icon-cc iconflushbegin'></i><i class='icon-cc-by iconflush'></i><i class='icon-cc-sa iconflushend'></i>4.0"
+++

<img class= "paper-plane" src="/img/paper-plane.svg">

<br>

<h3 class="text-info">Vielen Dank für Ihre Nachricht.</h3>

<h4 class="text-muted">Wir werden uns so bald als möglich bei Ihnen melden.</h4>

<br>
<br>

<a href="/" class="btn btn-small btn-template-main"><i class="icon-home"></i> Zur Startseite</a>

<br>
<br>
