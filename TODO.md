# TODOs

-   Overhaul Hugo content generation from R and integrate better with new [Quarto-built FOKUS Aarau report](https://gitlab.com/zdaarau/fokus_reports)

-   Find a way to source redirect URLs in `netlify.toml` from our "single source of truth", the fokus R package (or it's survey spec TOML files, respectively);
    maybe we just need to programmatically alter the `netlify.toml` file from R (using suitable sentinel comments or the like)?

-   Replace Google Site Search by [Stork](https://stork-search.net/)! Ideally, write a [Hugo module](https://gohugo.io/hugo-modules/) for the Stork index
    creation and everthing...

