var map = L.map('map', {
  center: [{{ .Site.Params.latitude }}, {{ .Site.Params.longitude }}],
  zoom: 13
