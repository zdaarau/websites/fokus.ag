# load magrittr
library(magrittr,
        include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))

# set options
options(
  ## project-specific options
  blogdown.subdir = "analysen",
  is.blogdown = TRUE,
  rmarkdown.html_dependency.header_attr = FALSE,

  # user-specific options
  path.repo.fa.fokus_aargau = getOption("path.repo.fa.fokus_aargau",
                                        default = switch(EXPR = Sys.info()[["user"]],
                                                         "salim" = fs::path_expand("~/Arbeit/ZDA/Git/c2d-zda/fokus_aargau"),
                                                         cli::cli_abort("{.field path.repo.fa.fokus_aargau} is unknown for user {.val {Sys.info()[['user']]}}")))
)

# define convenience fns
subhead <- function(text,
                    level = 4,
                    font_size = 14) {

  if (getOption("is.blogdown",
                default = FALSE)) {
    return(paste0(paste0(rep(x = "#",
                             times = level),
                         collapse = ""),
                  " ",
                  text))
  } else {
    return(kableExtra::text_spec(x = text,
                                 font_size = font_size,
                                 bold = TRUE))
  }
}
